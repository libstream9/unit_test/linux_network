#include <stream9/linux/socket.hpp>

#include "namespace.hpp"

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(socket_)

    BOOST_AUTO_TEST_CASE(case_1_)
    {
        lx::addrinfo ai1 { "www.google.com", "http", lx::tcp };

        auto s = lx::socket(ai1.family(), ai1.socktype(), ai1.protocol());
    }

    BOOST_AUTO_TEST_CASE(case_2_)
    {
        lx::addrinfo ai1 { "www.google.com", "http", lx::tcp };

        auto s = lx::socket(ai1);
    }

BOOST_AUTO_TEST_SUITE_END() // socket_

} // namespace testing
