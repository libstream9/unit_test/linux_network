#include "namespace.hpp"

#include <stream9/linux/addrinfo.hpp>
#include <stream9/linux/connect.hpp>
#include <stream9/linux/recv.hpp>
#include <stream9/linux/send.hpp>
#include <stream9/linux/socket.hpp>

#include <stream9/linux/epoll.hpp>
#include <stream9/linux/fd.hpp>
#include <stream9/string.hpp>
#include <stream9/string_view.hpp>

#include <iostream>

#define BOOST_TEST_MODULE main
#include <boost/test/included/unit_test.hpp>

namespace testing {

using st9::string;
using st9::string_view;

#if 0
BOOST_AUTO_TEST_CASE(case_X_)
{
    lx::epoll<> ep;
    //lx::addrinfo ai { "www.google.com", "http", lx::tcp };
    lx::addrinfo ai { "192.168.1.1", "http", lx::tcp };

    auto s = lx::socket(ai, SOCK_NONBLOCK);
    ep.add(s, 0);

    lx::connect_nothrow(s, ai);

    string_view req = "GET / HTTP/1.1\r\n\r\n";

    ep.modify(s, EPOLLOUT);
    while (!req.empty()) {
        auto events = ep.wait();
        BOOST_TEST(events.size() == 1);

        auto o_n = lx::send(s, req);
        if (!o_n) {
            throw st9::error { "send(2)", o_n.error() };
        }
        else {
            req.remove_prefix(*o_n);
        }
    }

    char buf[5*1024] {};

    string msg;
    ep.modify(s, EPOLLIN);
    while (true) {
        auto events = ep.wait();
        BOOST_TEST(events.size() == 1);

        auto o_n = lx::recv(s, buf);
        if (o_n) {
            if (*o_n == 0) {
                std::cout << "finish receiving data" << std::endl;
                ep.remove(s);
                break;
            }
            else {
                std::cout << "receive " << *o_n << " new data" << std::endl;

                msg.append(string_view(buf, buf + *o_n));
            }
        }
        else {
            throw st9::error { "recv(2)", o_n.error() };
        }
    }

    std::cout << msg << std::endl;
}
#endif
#if 0
st9::task<void, lx::errc>>
recv_all(fd_ref sockfd, buffer_array<4096>& buf_seq, int options = 0)
{
    while (true) {
        auto& buf = buf_seq.emplace();

        auto o_n = co_await lx::recv(s, buf);
        if (o_n) {
            if (*o_n == 0) {
                std::cout << "finish receiving data" << std::endl;
                break;
            }
            else {
                std::cout << "receive " << *o_n << " new data" << std::endl;

                buf.resize(*o_n);
            }
        }
        else {
            throw st9::error { "recv(2)", o_n.error() };
        }
    }
}

st9::task<outcome<void, lx::errc>>
send_all(fd_ref sockfd, array_view<char const> buf, int options = 0)
{
    while (!buf.empty()) {
        auto o_n = co_await lx::send(s, req, options);
        if (!o_n) {
            return o_n.error(); //TODO should return number of byte sent too.
        }
        else {
            req.remove_prefix(*o_n);
        }
    }

    return {};
}

st9::task<string>
slurp_http2(cstring_ptr node)
{
    lx::addrinfo ai { node, "http", lx::tcp };

    auto s = lx::socket(ai, SOCK_NONBLOCK);

    co_await lx::connect(s, ai);

    string_view req = "GET / HTTP/1.1\r\n\r\n";

    auto o_n = co_await send_all(s, req);

    buffer_sequence buf_seq;
    co_await recv_all(s, buf_seq);

    string rv = join(buf_seq);
}

st9::task<void>
slurp_http(cstring_ptr node)
{
    lx::addrinfo ai { node, "http", lx::tcp };

    auto s = lx::socket(ai, SOCK_NONBLOCK);

    co_await lx::connect(s, ai);

    string_view req = "GET / HTTP/1.1\r\n\r\n";

    auto o_n = co_await send_all(s, req);

    char buf[5*1024] {};
    string msg;

    while (true) {
        auto o_n = co_await lx::recv(s, buf);
        if (o_n) {
            if (*o_n == 0) {
                std::cout << "finish receiving data" << std::endl;
                break;
            }
            else {
                std::cout << "receive " << *o_n << " new data" << std::endl;

                msg.append(string_view(buf, buf + *o_n));
            }
        }
        else {
            throw st9::error { "recv(2)", o_n.error() };
        }
    }

    std::cout << msg << std::endl;
}
#endif

} // namespace testing

