#ifndef STREAM9_LINUX_NETWORK_TEST_SRC_NAMESPACE_HPP
#define STREAM9_LINUX_NETWORK_TEST_SRC_NAMESPACE_HPP

namespace stream9::linux {};
namespace stream9::json {};

namespace testing {

namespace st9 = stream9;
namespace json = st9::json;
namespace lx = st9::linux;

} // namespace testing

#endif // STREAM9_LINUX_NETWORK_TEST_SRC_NAMESPACE_HPP
