#include <stream9/linux/addrinfo.hpp>

#include "namespace.hpp"

#include <iostream>
#include <type_traits>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(addrinfo_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(std::is_nothrow_move_constructible_v<lx::addrinfo>);
        static_assert(std::is_nothrow_move_assignable_v<lx::addrinfo>);
        static_assert(std::is_nothrow_swappable_v<lx::addrinfo>);
        static_assert(std::is_nothrow_destructible_v<lx::addrinfo>);
    }

    BOOST_AUTO_TEST_CASE(constructor_1_)
    {
        lx::addrinfo ai1 { nullptr, "http", nullptr };

        auto r1 = json::value_from(ai1);
        BOOST_TEST(json::find_string(r1, (int)0, "ai_addr", "addr") == "::1"); //TODO prevent conversion from 0 to string_view
    }

    BOOST_AUTO_TEST_CASE(constructor_2_)
    {
        lx::addrinfo ai1 { "www.google.com" };

        BOOST_TEST(ai1.socktype() == SOCK_STREAM);
        BOOST_TEST(ai1.protocol() == IPPROTO_TCP);
    }

    BOOST_AUTO_TEST_CASE(constructor_3_)
    {
        lx::addrinfo ai1 { "www.google.com", "https" };

        BOOST_TEST(ai1.socktype() == SOCK_STREAM);
        BOOST_TEST(ai1.protocol() == IPPROTO_TCP);
    }

    BOOST_AUTO_TEST_CASE(constructor_4_)
    {
        lx::addrinfo ai1 { "8.8.8.8" };

        BOOST_TEST(ai1.family() == AF_INET);
        BOOST_TEST(ai1.socktype() == SOCK_STREAM);
        BOOST_TEST(ai1.protocol() == IPPROTO_TCP);
    }

BOOST_AUTO_TEST_SUITE_END() // addrinfo_

BOOST_AUTO_TEST_SUITE(addrinfo_ref_)

    BOOST_AUTO_TEST_CASE(concepts_)
    {
        static_assert(std::is_nothrow_copy_constructible_v<lx::addrinfo_ref>);
        static_assert(std::is_nothrow_copy_assignable_v<lx::addrinfo_ref>);
        static_assert(std::is_nothrow_move_constructible_v<lx::addrinfo_ref>);
        static_assert(std::is_nothrow_move_assignable_v<lx::addrinfo_ref>);
        static_assert(std::is_nothrow_swappable_v<lx::addrinfo_ref>);
        static_assert(std::is_nothrow_destructible_v<lx::addrinfo_ref>);
    }

    BOOST_AUTO_TEST_CASE(constructor_)
    {
        lx::addrinfo ai1 { "www.google.com", "https" };

        lx::addrinfo_ref ref1 { ai1 };

        BOOST_TEST(ref1.socktype() == SOCK_STREAM);
        BOOST_TEST(ref1.protocol() == IPPROTO_TCP);
    }

BOOST_AUTO_TEST_SUITE_END() // addrinfo_ref_



} // namespace testing
