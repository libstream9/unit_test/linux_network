#include <stream9/linux/connect.hpp>

#include "namespace.hpp"

#include <stream9/linux/addrinfo.hpp>
#include <stream9/linux/socket.hpp>

#include <boost/test/unit_test.hpp>

namespace testing {

BOOST_AUTO_TEST_SUITE(connect_)

    BOOST_AUTO_TEST_CASE(basic_1_)
    {
        lx::addrinfo ai { "www.google.com", "http" };

        auto fd = lx::socket(ai);

        lx::connect(fd, ai.addr(), ai.addrlen());
    }

    BOOST_AUTO_TEST_CASE(basic_2_)
    {
        lx::addrinfo ai { "www.google.com", "http" };

        auto fd = lx::socket(ai);

        lx::connect(fd, ai);
    }

BOOST_AUTO_TEST_SUITE_END() // connect_

} // namespace testing
